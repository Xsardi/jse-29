package ru.t1.tbobkov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
