package ru.t1.tbobkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.repository.IProjectRepository;
import ru.t1.tbobkov.tm.api.service.IProjectService;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.tbobkov.tm.exception.field.*;
import ru.t1.tbobkov.tm.exception.user.UserAccessDeniedException;
import ru.t1.tbobkov.tm.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        if (!userId.equals(project.getUserId())) throw new UserAccessDeniedException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        if (!userId.equals(project.getUserId())) throw new UserAccessDeniedException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusIncorrectException();
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        if (!userId.equals(project.getUserId())) throw new UserAccessDeniedException();
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final Status status) {
        if (status == null) throw new StatusIncorrectException();
        if (index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        if (!userId.equals(project.getUserId())) throw new UserAccessDeniedException();
        project.setStatus(status);
        return project;
    }

}